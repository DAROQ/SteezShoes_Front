const path = require('path');
const {CleanWebpackPlugin} = require("clean-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require("copy-webpack-plugin");

module.exports = (env, argv) => {
    return {
        entry: path.resolve(__dirname, 'src/index.js'),
        output: {
            filename: '[fullhash].bundle.js',
            path: path.resolve(__dirname, 'dist'),
            publicPath: "/" //Para que webpack no interfiera con las rutas hijas y le delegue su manejo a react router.
        },
        resolve: {
            extensions: ['.js', '.jsx']
        },
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env', '@babel/preset-react']
                    }
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader']
                },
                {
                    test: /\.(svg|png|jpe?g|gif)$/i,
                    loader: 'file-loader',
                    options: {
                        esModule: false
                    }
                },
                {
                    test: /\.(eot|woff|woff2|ttf)$/,
                    loader: 'url-loader',
                    options: {
                        limit: 30000,
                        name: '[name]-[hash].[ext]',
                        outputPath: 'Fonts/'
                    }
                }
            ]
        },
        plugins: [
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                template: argv.mode === 'development'? path.resolve(__dirname, 'public/index.html'): path.resolve(__dirname, 'public/index.prod.html'),
                favicon: path.resolve(__dirname, 'public/favicon.ico'),
                inject: true
            }),
            new CopyWebpackPlugin({ // Copia archivos desde el codigo fuente a la carpeta de compilación para producción
                patterns: [
                  { from: path.resolve(__dirname, 'public/env.js') }              
                ]
                
            })
        ],
        devtool: 'eval',
        devServer: {
            host: '0.0.0.0',
            port: 3000,
            historyApiFallback: true, // Esto es para que se delegue a React Router las rutas de la app
            hot: true //live-reload
        }
    };

}; 