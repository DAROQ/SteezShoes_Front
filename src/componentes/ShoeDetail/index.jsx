import React, { useState, useContext, useEffect } from 'react';
import {useParams} from 'react-router-dom';
import { useHistory } from "react-router-dom";

// Importar estilos del componente
import './styles.css'

// Importar contexto del carrito de compras
import carritoContext from '../../context/carrito/CarritoContext';

// Importar la lista de zapatos
import shoes from '../../data/shoes.json';
// Importar peticiones API REST
import { getZapatoByID, getDetalleZapato } from '../../utils/SteezRequests';

// Componentes Bootstrap
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';

function ShoeDetail(props) {

  const CarritoContext = useContext(carritoContext);

  let { id } = useParams();
  // let shoeId = id;
  // Declaración de los estados del componente
  // const [shoe, setShoe] = useState(selectedShoe); 
  const [shoe, setShoe] = useState(); 
  const [talla, setTalla] = useState(0);
  const [cantidad, setCantidad] = useState(1);
  
  let navegacion = useHistory();
  useEffect(() => {
    getZapatoByID(id).then(data => {
      getDetalleZapato(data).then(detalle => {
        data.detalle = detalle;
        setShoe(data)
      });
    });
  }, []);
  // const selectedShoe = shoes.find( ({ id }) => id == shoeId );

    

    function irALaTienda() {        
        navegacion.push("/");
    }

    
    function agregarAlCarrito() {
      shoe.talla = talla;
      shoe.cantidad = cantidad;
      let shoeCarrito = { ...shoe }; // Se hace una copia para que no se mande como referencia, sino como un nuevo objeto independiente.
      CarritoContext.addProducto(shoeCarrito);
      props.mostrarAlerta();
    }

    function cambiarCantidad(sumando) {
      let cantidadNumerica = parseInt(cantidad);
      if(sumando) {
        setCantidad(isNaN(cantidadNumerica) ? 1: cantidadNumerica + 1);
      } else {
        setCantidad(isNaN(cantidadNumerica) ? 1: cantidadNumerica - 1);
      }
    }

    function handleCantidadInput(e) {
      e.currentTarget.value ? e.currentTarget.value >= 1 ? setCantidad(e.currentTarget.value) : setCantidad(1) : setCantidad(""); 

    }

    return (
        <div>            
            { shoe ? (                                        
              <Row >
                <Col md={9}>
                  {shoe.modelo != "FOREVER FLOATRIDE ENERGY" ? shoe.modelo != "CUSTOMS CAMO AUTHENTIC" ? (
                    <>
                    {/* <img className="detail-img" src={require('../../images/' + shoe.detalle["detail_img_left"])} alt=""/> 
                    <img className="detail-img" src={require('../../images/' + shoe.detalle["detail_img_right"])} alt=""/> */}
                    <img className="detail-img" src={ shoe.detalle["detail_img_left"] } alt=""/> 
                    <img className="detail-img" src={ shoe.detalle["detail_img_right"] } alt=""/>
                    </>
                  ):(
                    <>
                    {/* <img className="detail-img-vans" src={require('../../images/' + shoe.detalle["detail_img"])} alt=""/>  */}
                    <img className="detail-img-vans" src={ shoe.detalle["detail_img"] } alt=""/>
                    </>
                  ):(
                    <>
                    {/* <img className="detail-img-reebok" src={require('../../images/' + shoe.detalle["detail_img_left"])} alt=""/> 
                    <img className="detail-img-reebok" src={require('../../images/' + shoe.detalle["detail_img_right"])} alt=""/> */}
                    <img className="detail-img-reebok" src={ shoe.detalle["detail_img_left"] } alt=""/> 
                    <img className="detail-img-reebok" src={ shoe.detalle["detail_img_right"] } alt=""/>
                    </>
                  )}
                  
                  
                </Col>
                <Col md={3} className="texto">
                  <h3>{shoe.modelo}</h3>
                  <h5>${shoe.precio.toLocaleString()}</h5>
                  Tallas:
                  <br/>                  
                  <ToggleButtonGroup className="tallas-radio-group" type="radio" name="tallas" > 
                    
                      <ToggleButton variant="outline-dark" value={36} onChange={(e) => setTalla(e.currentTarget.value)}>36</ToggleButton>
                      <ToggleButton variant="outline-dark" value={38} onChange={(e) => setTalla(e.currentTarget.value)}>38</ToggleButton>          
                      <ToggleButton variant="outline-dark" value={40} onChange={(e) => setTalla(e.currentTarget.value)}>40</ToggleButton>
                      <ToggleButton variant="outline-dark" value={42} onChange={(e) => setTalla(e.currentTarget.value)}>42</ToggleButton>                                      
                                      
                  </ToggleButtonGroup>                  
                  <span className="texto text-justify" style={{ display: "block", marginTop: 15 +"px",  marginBottom: "15px"}}>{shoe.detalle?.descripcion}</span>
                  <Row style={{paddingLeft:"15px"}}>
                    <label htmlFor="cantidad" style={{paddingTop: "5px"}}>Cantidad:</label> 
                    <InputGroup className="mb-3" style={{width:120 + "px", marginLeft: 10 + "px"}}>
                      <InputGroup.Prepend>
                        <Button variant="outline-dark" className="shadow-none" disabled={cantidad < 2} onClick={() => cambiarCantidad(false)}>-</Button>
                      </InputGroup.Prepend>                   
                      <FormControl style={{textAlign: "center"}}
                        value={cantidad}
                        onChange={handleCantidadInput}
                        placeholder="0"
                        aria-label="10"
                        aria-describedby="basic-addon2"
                        id="cantidad"
                        name="cantidad" 
                        className="numberInput" 
                        type="number" 
                        min="1"                     
                      />
                      <InputGroup.Append>
                        <Button variant="outline-dark" className="shadow-none" onClick={() => cambiarCantidad(true)}>+</Button>
                      </InputGroup.Append>
                    </InputGroup>                  
                  </Row>
                  <Button className='añadir-carrito-button' variant="outline-success" onClick={agregarAlCarrito} disabled={ talla === 0  || cantidad < 1}>{talla != 0 ? cantidad >= 1 ? "Añadir": "Ingrese una cantidad valida" :"Por favor seleccione una talla"}</Button>                
                  <Button className='añadir-carrito-button' variant="outline-dark" onClick={irALaTienda} >Seguir Comprando</Button>                
                </Col>
              </Row>
             
            ): ( 
              <span>No se encontro el artículo</span>
            )}
            
        </div>
        
    );
    

}

export default ShoeDetail;