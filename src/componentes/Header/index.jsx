import React, { useState, useContext } from 'react';
import { Link } from "react-router-dom";

// Hoja de estilos
import './styles.css';

// Componentes Bootstrap
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';
import Button from 'react-bootstrap/Button';
import Badge from 'react-bootstrap/Badge';

// Logo Para el Brand del Navbar
import logoSteezShoes from '../../images/logo-steezShoes.png';

// Importar contexto del carrito de compras
import carritoContext from '../../context/carrito/CarritoContext';

function Header() {

    const CarritoContext = useContext(carritoContext);

    return (
        <Navbar sticky="top" bg="my-dark" variant="dark" collapseOnSelect expand="md">
            <Navbar.Brand className="brand-name" as={Link} to="/">
                <img className="d-inline-block align-top" alt="Logo Steez Shoes" src={logoSteezShoes} width="37" height="37" />{' '}
                Steez Shoes
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="brand-name mr-auto">
                    <Nav.Link href="#new">Novedades</Nav.Link>
                    <Nav.Link href="#brands">Marcas</Nav.Link>
                    <Nav.Link as={Link} to="/shopping-cart">Carrito</Nav.Link>{CarritoContext.cantidadProductos > 0 ? <Badge className="carrito-badge" pill variant="danger">{CarritoContext.cantidadProductos}</Badge>:''}
                </Nav>
                <Form inline className="buscador">
                    <FormControl type="text" placeholder="Marcas, Modelos, Estilos..." className="mr-sm-2" />
                    <Button variant="outline-danger">Buscar</Button>
                </Form>
            </Navbar.Collapse>                                 
        </Navbar>
    );

}

export default Header;