import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from "react-router-dom";

// Hoja de estilos
import './Shoe.css';

// Contexto del carrito de compras

import carritoContext from '../context/carrito/CarritoContext';

// Componentes Bootstrap
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import ToggleButtonGroup from 'react-bootstrap/ToggleButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';

function Shoe({ shoe, mostrarAlerta }) {

    const CarritoContext = useContext(carritoContext);
    const [showModal, setShowModal] = useState(false);
    const [talla, setTalla] = useState(0);
    const [cantidad, setCantidad] = useState(1);

    let navegacion = useHistory();

    function mostrarDetalles() {        
        navegacion.push("/detail/" + shoe.id);
    }

    function mostrarModal() {
        setShowModal(true);
    }

    function agregarAlCarrito(){
        const carritoShoe = { ...shoe };
        carritoShoe.talla = talla;
        carritoShoe.cantidad = cantidad;
        CarritoContext.addProducto(carritoShoe);  
        mostrarAlerta(); 
        setShowModal(false);             
    }

    function cambiarCantidad(sumando) {
        let cantidadNumerica = parseInt(cantidad);
        if(sumando) {
          setCantidad(isNaN(cantidadNumerica) ? 1: cantidadNumerica + 1);
        } else {
          setCantidad(isNaN(cantidadNumerica) ? 1: cantidadNumerica - 1);
        }
      }
  
      function handleCantidadInput(e) {
        e.currentTarget.value ? e.currentTarget.value >= 1 ? setCantidad(e.currentTarget.value) : setCantidad(1) : setCantidad(""); 
  
      }

      function cerrarModal(){
          setShowModal(false);
      }


    return (
        <>
        <Card className="texto">
            {/* <Card.Img variant="top" src={require('../images/' + shoe.foto)}  /> */}
            <Card.Img variant="top" src={ shoe.foto }  />
            <Card.Body>
                <Card.Title>{shoe.modelo}</Card.Title>
                <Card.Text>
                    {shoe.marca} <br/>
                    ${shoe.precio.toLocaleString()}
                </Card.Text>
            </Card.Body>
            <Card.Footer>
                <Button className='shoe-button' variant="outline-success" onClick={mostrarModal}>Añadir</Button>
                <Button className='shoe-button' variant="outline-dark" style={{marginTop: 10 + 'px'}} onClick={mostrarDetalles}>Más Detalles</Button>
            </Card.Footer>
        </Card>

        <Modal className="texto" show={showModal} onHide={cerrarModal}>
            <Modal.Header closeButton>
                <Modal.Title>Agregar al carrito</Modal.Title>
            </Modal.Header>
            <Modal.Body>
            Tallas:
                  <br/>                  
                  <ToggleButtonGroup className="shoe-tallas-radio-group" type="radio" name="tallas" > 
                    
                      <ToggleButton variant="outline-dark" value={36} onChange={(e) => setTalla(e.currentTarget.value)}>36</ToggleButton>
                      <ToggleButton variant="outline-dark" value={38} onChange={(e) => setTalla(e.currentTarget.value)}>38</ToggleButton>          
                      <ToggleButton variant="outline-dark" value={40} onChange={(e) => setTalla(e.currentTarget.value)}>40</ToggleButton>
                      <ToggleButton variant="outline-dark" value={42} onChange={(e) => setTalla(e.currentTarget.value)}>42</ToggleButton>                                      
                                      
                  </ToggleButtonGroup>                  
                  
                  <Row style={{paddingLeft:"15px", marginTop: "15px"}}>
                    <label htmlFor="cantidad" style={{paddingTop: "5px"}}>Cantidad:</label> 
                    <InputGroup className="mb-3" style={{width:120 + "px", marginLeft: 10 + "px"}}>
                      <InputGroup.Prepend>
                        <Button variant="outline-dark" className="shadow-none" disabled={cantidad < 2} onClick={() => cambiarCantidad(false)}>-</Button>
                      </InputGroup.Prepend>                   
                      <FormControl style={{textAlign: "center"}}
                        value={cantidad}
                        onChange={handleCantidadInput}
                        placeholder="0"
                        aria-label="10"
                        aria-describedby="basic-addon2"
                        id="cantidad"
                        name="cantidad" 
                        className="numberInput" 
                        type="number" 
                        min="1"                     
                      />
                      <InputGroup.Append>
                        <Button variant="outline-dark" className="shadow-none" onClick={() => cambiarCantidad(true)}>+</Button>
                      </InputGroup.Append>
                    </InputGroup>                  
                  </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="outline-dark" onClick={cerrarModal}>
                    Seguir Comprando
                </Button>                
                <Button className='añadir-carrito-shoe-button' variant="outline-success" onClick={agregarAlCarrito} disabled={ talla === 0  || cantidad < 1}>{talla != 0 ? cantidad >= 1 ? "Añadir": "Ingrese una cantidad valida" :"Por favor seleccione una talla"}</Button>                
            </Modal.Footer>
        </Modal>
        </>
    );

}

Shoe.defaultProps = {
    id: 0,
    modelo: 'Modelo',
    marca: 'Marca',
    precio: 0,
    foto: '',
};
  
Shoe.propTypes = {
    id: PropTypes.number.isRequired,
    modelo: PropTypes.string.isRequired,
    marca: PropTypes.string.isRequired,
    precio: PropTypes.number.isRequired,
    foto: PropTypes.string.isRequired
};

export default Shoe;