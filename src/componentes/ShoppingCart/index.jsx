import React, { useState, useContext} from 'react';
import { useHistory } from "react-router-dom";

// Importar Componentes de Bootstrap
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

// Importar estilos del componente
import './styles.css';

// Importar contexto del carrito de compras
import carritoContext from '../../context/carrito/CarritoContext';

function ShoppingCart() {

    const CarritoContext = useContext(carritoContext);
    
    let navegacion = useHistory();

    function irALaTienda() {        
        navegacion.push("/");
    }

    function removerDelCarrito (id) {
        CarritoContext.delProducto(id);
    }


    return (
        <>
        { CarritoContext.productos.length > 0 ? ( 
            <>
            {CarritoContext.productos.map((shoe, index) => (
                <Row key={shoe.id} className="texto">
                    <Col md={2}>
                        {/* <img className="carrito-img" src={require("../../images/" + shoe.foto)} />   */}
                        <img className="carrito-img" src={ shoe.foto } />
                    </Col>
                    <Col md={7} >
                        <p>{shoe.modelo}</p>
                        <p>Talla: {shoe.talla}</p>
                        <Button variant="outline-danger" onClick={() => removerDelCarrito(shoe.id)}>Quitar del Carrito</Button>
                    </Col>                  
                    <Col md={3}>
                        <div className="text-right">
                            <br />
                            <p>Precio: ${shoe.precio.toLocaleString()}</p>
                            <p>Cantidad: {shoe.cantidad}</p>
                            <p>Subtotal: ${(shoe.precio * shoe.cantidad).toLocaleString()} </p>
                        </div>
                    </Col>
                    {index + 1 !== CarritoContext.productos.length  ? <div className="h-divider"></div>: <div></div>}                                      
                </Row>        
            ))}
            <p className="texto text-right"><span style={{fontStyle: 'italic', fontWeight: 'bold'}}>Total:</span> ${CarritoContext.total.toLocaleString()}</p> 
            </>
        ): (
            <div className="texto">
                <p className="text-center"> El carrito esta vacio </p>
                <div className="center">
                 <Button className='añadir-carrito-shoe-button ' variant="outline-success" onClick={irALaTienda} >Buscar unos Steez</Button>                
                </div>                
            </div>
            
        )} 
        </>
        
        
    );

}

export default ShoppingCart;