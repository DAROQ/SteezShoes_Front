import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';

// Importar Componentes de Bootstrap
import Row from 'react-bootstrap/Row';

// Importar Componentes de Aplicación
import Shoe from '../Shoe'; 

// Importar estilos del componente
import './styles.css'

// Importar Peticiones API REST
import { getZapatos } from '../../utils/SteezRequests';

function ShoeList({mostrarAlerta}) {
    // Declaración de los estados del componente
    const [state, setState] = useState({
        shoes: [],
    });  
    useEffect(() => {
      getZapatos().then(data => setState({
        shoes: data.results
      }));      
    }, []);

    return (
        
        <Row xs={1} sm={2} md={3} >
            { state.shoes.map(shoe => (
            <div className="shoe-card" key={shoe.id}>
              <Shoe shoe={shoe} mostrarAlerta={mostrarAlerta} />
            </div>
            ))}
        </Row>
    );
    

}

// ShoeList.defaultProps = {
//     shoes: [
//       {
//         id: 0,
//         modelo: 'Modelo',
//         marca: 'Marca',
//         precio: 0,
//         foto: ''
//       }
//     ]
//   };
  
//   ShoeList.propTypes = {
//     shoes: PropTypes.arrayOf(
//       PropTypes.shape({
//         id: PropTypes.number,
//         modelo: PropTypes.string,
//         marca: PropTypes.string,
//         precio: PropTypes.number,
//         foto: PropTypes.string,
//       })
//     ).isRequired
//   };

export default ShoeList;