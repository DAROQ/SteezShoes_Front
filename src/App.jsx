import React, { useState } from 'react';
import { Switch, Route } from "react-router-dom";

// Hoja de estilos
import './App.css';

// Componentes Bootstrap
import Container from 'react-bootstrap/Container';
import Alert from 'react-bootstrap/Alert';

// Componentes Aplicación
import Header from './componentes/Header';
import ShoeList from './componentes/ShoeList';
import ShoeDetail from './componentes/ShoeDetail';
import ShoppingCart from './componentes/ShoppingCart';

// Contexto para el carrito de compras
import CarritoState from './context/carrito/CarritoState';

// Data %Descomentar para usar datos locales en lugar del API%
// import shoes from './data/shoes.json';

function App() {

    const [showAlert, setShowAlert] = useState(false);

    function alertaProductoAgregado() {
        setShowAlert(true);
    }

    return (   
                <CarritoState>                    
                    <Header />   
                    <div className="alerta texto">
                        <Alert className="" variant="my-dark" show={showAlert} onClose={() => setShowAlert(false)} dismissible>
                            <Alert.Heading>Look at that steez!</Alert.Heading>
                            <p>
                                El producto ha sido añadido al carrito de compras.
                            </p>    
                        </Alert>
                    </div>                                                                    
                    <Container>                    
                        <h1 className="text-center titulo">Let The World Know Your Steez</h1> 
                        <p className="text-center texto mt-3">Lo sabemos, lo tienes. <span className="texto" style={{fontStyle: 'italic', fontWeight: 'bold'}}>Style with Ease</span> es lo que haces. Pero podemos ayudarte a hacerlo aún mas facil.</p>
                        <Switch>                            
                            <Route exact path="/" >
                                <ShoeList mostrarAlerta={alertaProductoAgregado} />
                            </Route>
                            <Route path="/detail/:id" >
                                <ShoeDetail mostrarAlerta={alertaProductoAgregado} />
                            </Route>
                            <Route path="/shopping-cart" >
                                <ShoppingCart />                            
                            </Route>                                                                          
                        </Switch>                
                    </Container> 
                </CarritoState>           
                  
    );
}

export default App;