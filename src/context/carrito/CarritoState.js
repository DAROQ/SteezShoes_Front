import React, { useReducer } from 'react';

// Importar el contexto
import CarritoContext from './CarritoContext';

// Importar el reducer
import CarritoReducer from './CarritoReducer';

// Importar acciones
import { ADD_PRODUCTO, DEL_PRODUCTO } from './types';

const CarritoState = (props) => {
    let initialState = {
        productos: [],
        lastId: 0,
        total: 0,
        cantidadProductos: 0
    }

    const [state, dispatch] = useReducer(CarritoReducer, initialState);
    
    const addProducto = (producto) => {
        let productos = [...state.productos];
        let lastId = state.lastId + 1; 
        producto.id = lastId; 
        productos.push(producto);
        let cantidadProductos = state.cantidadProductos + 1;
        let subtotal = producto.precio * producto.cantidad;
        let total = state.total + subtotal; 
        dispatch({ type: ADD_PRODUCTO, payload: { productos, lastId, total, cantidadProductos } });
    };

    const delProducto = (id) => {
        let productos = [...state.productos];
        let indexProducto, productoAEliminar;
        productoAEliminar = productos.find((producto, index) => {
            if(producto.id == id) {
                indexProducto = index;
                return true;
            }
            return false;
        }); 
        productos.splice(indexProducto, 1);    
        let subtotal = productoAEliminar.precio * productoAEliminar.cantidad;
        let total = state.total - subtotal;       
        let cantidadProductos = state.cantidadProductos - 1;
        dispatch({ type: DEL_PRODUCTO, payload: { productos, total, cantidadProductos } });
    };

    return (
        <CarritoContext.Provider
            value = {
                {
                    productos: state.productos,
                    total: state.total,
                    cantidadProductos: state.cantidadProductos,
                    addProducto,
                    delProducto
                }
            }
        >
            { props.children }
        </CarritoContext.Provider>
    );

};

export default CarritoState;


