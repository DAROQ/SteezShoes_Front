// Esta es la declaración de los tipos de acciones que cambian el estado del carrito

export const ADD_PRODUCTO = "ADD_PRODUCTO";
export const DEL_PRODUCTO = "DEL_PRODUCTO";
export const VACIAR_CARRITO = "VACIAR_CARRITO"; 