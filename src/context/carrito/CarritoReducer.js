import { ADD_PRODUCTO, DEL_PRODUCTO, VACIAR_CARRITO } from "./types";

export default (state, action) => {
    const { payload, type } = action;

    switch (type) {
        case ADD_PRODUCTO:
            return {
                ...state,
                productos: payload.productos,
                lastId: payload.lastId,
                total: payload.total,
                cantidadProductos: payload.cantidadProductos,
            };
        case DEL_PRODUCTO:
            return {
                ...state,
                productos: payload.productos,
                total: payload.total,
                cantidadProductos: payload.cantidadProductos
            };
        default:
            return state;
    }

};