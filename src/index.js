import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

// La navegación con react-router no deja el scroll de la pagina en la parte superior
// ... si no que lo deja igual que como estaba antes de que cambie el path
// para resolver esto se usa el siguiente componente
import ScrollToTop from './componentes/ScrollToTop';


// Importar Componente principal
import App from './App';

//Importar Estilos Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';

ReactDom.render(
    <BrowserRouter> 
        <ScrollToTop />   
        <App />
    </BrowserRouter>, 
    document.getElementById('root')
);