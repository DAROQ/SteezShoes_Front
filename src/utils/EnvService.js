function EnvService() {    
    // Se crea el objeto que contiene las variables de entorno
    const env = {
        apiURL: "http://localhost:8000", // Se debe colocar sin slash final
        debugMode: true
    };
    // La anterior es la configuración para el ambiente de desarrollo.  
    
    // Leer las variables de entorno del objeto window del navegador.
    const browserWindow = window || {};
    const browserWindowEnv = browserWindow['__env'] || {};
    
    // Nota: En el ambiente de desarrollo no deberia existir window.__env, asi que el objeto env...
    // ... deberia quedar con la configuración de arriba.
    
    // Poblar el objeto env con las variables de entorno
    for (const key in browserWindowEnv) {
        if (browserWindowEnv.hasOwnProperty(key)) {
            env[key] = window['__env'][key];
        }
    }
    
    return env;
};

export default EnvService;
