import Client from './HTTPClient';
import EnvService from './EnvService';

const env = EnvService();
export const BASE_URL = env.apiURL;
const SteezApi = new Client(`${BASE_URL}/steez/`);

export function getZapatos() {
    return SteezApi.get('zapatos');
}

export function getZapatoByID(id) {
    return SteezApi.get(`zapatos/${id}`);
}

export function getDetalleZapato(zapato) {
    return SteezApi.get(`detalles/${zapato.detalle}`);
}